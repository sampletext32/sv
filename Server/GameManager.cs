﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebSocketSharp;
using WebSocketSharp.Server;

namespace Server
{
    public static class GameManager
    {
        private static readonly List<Player> Players = new List<Player>();
        private static WebSocketBehavior _webSocketBehavior;

        public static void SetWebSocketBehavior(WebSocketBehavior behavior)
        {
            _webSocketBehavior = behavior;
        }

        public static void ProcessNewConnection(WebSocket ws, string id)
        {
            var pl = new Player(ws, id);
            pl.OnWsMessage += OnPlayerWsMessage;
            pl.OnWsClosed += OnPlayerWsClosed;
            pl.OnWsError += OnPlayerWsError;
            Players.Add(pl);
        }

        private static void OnPlayerWsMessage(Player player, MessageEventArgs e)
        {
            if (e.IsPing)
            {
                player.LastPingTime = DateTime.Now;
            }
            else if (e.IsText)
            {
                var jObject = JObject.Parse(e.Data);
                ProcessPlayerMessage(player, jObject);
            }
        }

        private static void OnPlayerWsError(Player player, ErrorEventArgs e)
        {
            Console.WriteLine("An error occured with player " + player.Id);
        }

        private static void OnPlayerWsClosed(Player player, CloseEventArgs e)
        {
            Console.WriteLine("Connection with player " + player.Id + " was closed");
            Players.Remove(player);
        }

        private static void ProcessPlayerMessage(Player player, JObject message)
        {
            var area = message["area"].ToString();
            switch (area)
            {
            }
        }
    }
}