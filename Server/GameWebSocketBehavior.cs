﻿using System;
using WebSocketSharp;
using WebSocketSharp.Server;

namespace Server
{
    public class GameWebSocketBehavior : WebSocketBehavior
    {
        public event Action<WebSocket, string> Connected;

        protected override void OnOpen()
        {
            OutReRaiseOnOpen(Context.WebSocket, ID);
        }

        private void OutReRaiseOnOpen(WebSocket webSocket, string iD)
        {
            Connected?.Invoke(webSocket, iD);
        }
    }
}