﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebSocketSharp;

namespace Server
{
    public class Player
    {
        private WebSocket _webSocket;

        public string Id { get; private set; }

        public DateTime LastPingTime;

        public event Action<Player, MessageEventArgs> OnWsMessage;
        public event Action<Player, CloseEventArgs> OnWsClosed;
        public event Action<Player, ErrorEventArgs> OnWsError;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="webSocket">Already Connected Socket</param>
        /// <param name="id">WebSocketSharp Connection Id</param>
        public Player(WebSocket webSocket, string id)
        {
            _webSocket = webSocket;
            _id = id;
            LastPingTime = DateTime.Now;
            _webSocket.EmitOnPing = true;
            _webSocket.OnMessage += InnerOnMessage;
            _webSocket.OnClose += InnerOnClose;
            _webSocket.OnError += InnerOnError;
        }

        private void InnerOnError(object sender, ErrorEventArgs e)
        {
            OnWsError?.Invoke(this, e);
        }

        private void InnerOnClose(object sender, CloseEventArgs e)
        {
            OnWsClosed?.Invoke(this, e);
        }

        private void InnerOnMessage(object sender, MessageEventArgs e)
        {
            OnWsMessage?.Invoke(this, e);
        }

        public void SendAsync(string message)
        {
            _webSocket.SendAsync(message, null);
        }
    }
}