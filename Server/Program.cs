﻿using System;
using WebSocketSharp;
using WebSocketSharp.Server;

namespace Server
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            var behavior = new GameWebSocketBehavior();
            GameManager.SetWebSocketBehavior(behavior);
            behavior.Connected += GameManager.ProcessNewConnection;

            var server = new WebSocketServer("ws://localhost:1000");

            server.AddWebSocketService("/", () => behavior);// ("/"=actual url)
            server.Start();

            Console.WriteLine("Server started");
            Console.WriteLine("Press any key to stop the server...");
            Console.ReadKey(true);

            server.Stop(CloseStatusCode.Normal, "Server closed");
        }
    }
}