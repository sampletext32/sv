﻿using System;
using Newtonsoft.Json.Linq;
using UnityEngine;
using WebSocketSharp;

namespace Assets.Scripts
{
    public class GameManager
    {
        private static WebSocket _webSocket;

        public static DateTime LastPingTime { get; private set; }

        public static void Init()
        {
            _webSocket = new WebSocket("ws://localhost:1000");
            _webSocket.OnOpen += OnOpen;
            _webSocket.OnError += OnError;
            _webSocket.OnClose += OnClose;
            _webSocket.EmitOnPing = true;
            _webSocket.OnMessage += OnMessage;
            _webSocket.ConnectAsync();
        }

        private static void OnMessage(object sender, MessageEventArgs e)
        {
            if (e.IsPing)
            {
                LastPingTime = DateTime.Now;
            }
            else if (e.IsText)
            {
                var jObject = JObject.Parse(e.Data);
                ProcessServerMessage(jObject);
            }
        }

        private static void OnClose(object sender, CloseEventArgs e)
        {
            Debug.Log(e.WasClean ? "Connection closed clearly" : "Connection lost");
        }

        private static void OnError(object sender, ErrorEventArgs e)
        {
            Debug.Log("Error: " + e.Message);
        }

        private static void OnOpen(object sender, EventArgs e)
        {
            Debug.Log("Connected");
        }

        private static void ProcessServerMessage(JObject message)
        {
            var area = message["area"].ToString();
            switch (area)
            {
            }
        }
    }
}